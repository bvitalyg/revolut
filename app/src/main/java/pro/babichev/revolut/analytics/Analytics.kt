package pro.babichev.revolut.analytics

import android.util.Log
import io.reactivex.functions.Consumer
import pro.babichev.revolut.view.ItemView

internal object Analytics : Consumer<ItemView.Event> {

    private const val TAG = "Analytics"

    // this is a mock class to show how easy it is to add analytics tracking
    override fun accept(event: ItemView.Event)  {
        when(event) {
            is ItemView.Event.Click -> Log.d(TAG, event.toString())
            is ItemView.Event.AmountUpdated ->Log.d(TAG, event.toString())
        }
    }

}
package pro.babichev.revolut

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import pro.babichev.revolut.Feature.*
import pro.babichev.revolut.models.Currency
import pro.babichev.revolut.models.CurrencyId
import pro.babichev.revolut.models.Rate
import pro.babichev.revolut.provider.RatesRepository
import java.math.BigDecimal

class FeatureTests {

    // since feature is only a state machine we need to check transitions between states only

    private val base = "base"
    private val subscribeScheduler = Schedulers.trampoline()
    private val observeScheduler = Schedulers.trampoline()

    private val repository = mock<RatesRepository>()

    @Test
    fun `initial state is null`() {
        whenever(repository.invoke(any())).thenReturn(Observable.never())
        val feature = createFeature()
        assertNull(feature.state.items)
    }

    @Test
    fun `initial data is set from repo without base`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1"), createRate("2"))
        ))
        val feature = createFeature()
        val items = feature.state.items!!
        assertEquals(createItem("1"), items[0])
        assertEquals(createItem("2"), items[1])
    }

    @Test
    fun `initial data is set from repo with base`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1"), createRate("base"))
        ))
        val feature = createFeature()

        val items = feature.state.items!!
        assertEquals(createItem("base"), items[0])
        assertEquals(createItem("1"), items[1])
    }

    @Test
    fun `update data affects rates for everything`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1", 1.0), createRate("2", 2.0)),
                listOf(createRate("1", 1.1), createRate("2", 2.1))
        ))
        val feature = createFeature()

        val items = feature.state.items!!
        assertEquals(createItem(id = "1", rate = 1.1), items[0])
        assertEquals(createItem(id = "2", rate = 2.1), items[1])
    }

    @Test
    fun `update data affects values for everything except active`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1", 1.0), createRate("2", 4.0)),
                listOf(createRate("1", 0.5), createRate("2", 4.0))
        ))
        val feature = createFeature()

        feature.accept(Wish.UpdateAmount("1", "1"))

        val items = feature.state.items!!
        assertEquals(createItem(id = "1", rate = 0.5, value = BigDecimal(1)), items[0])
        assertEquals(createItem(id = "2", rate = 4.0, value = BigDecimal(8)), items[1])
    }

    @Test
    fun `update amount does nothing on empty feature`() {
        whenever(repository.invoke(any())).thenReturn(Observable.never())
        val feature = createFeature()
        feature.accept(Wish.UpdateAmount("1", "1"))
        assertNull(feature.state.items)
    }

    @Test
    fun `update amount brings item to up`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1"),
                        createRate("2"),
                        createRate("3")
                )))

        val feature = createFeature()
        feature.accept(Wish.UpdateAmount("2", null))

        val items = feature.state.items!!
        assertEquals(createItem(id = "2"), items[0])
        assertEquals(createItem(id = "1"), items[1])
        assertEquals(createItem(id = "3"), items[2])
    }

    @Test
    fun `update amount brings item to up and then put it back to place`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1"),
                        createRate("2"),
                        createRate("3")
                )))

        val feature = createFeature()
        feature.accept(Wish.UpdateAmount("3", null))
        feature.accept(Wish.UpdateAmount("2", null))

        val items = feature.state.items!!
        assertEquals(createItem(id = "2"), items[0])
        assertEquals(createItem(id = "1"), items[1])
        assertEquals(createItem(id = "3"), items[2])
    }

    @Test
    fun `update amount makes conversion`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1", 0.5), createRate("2", 2.0))
        ))

        val feature = createFeature()
        feature.accept(Wish.UpdateAmount("1", "1"))
        val items = feature.state.items!!
        assertEquals(createItem(id = "1", value = BigDecimal.ONE, rate = 0.5), items[0])
        assertEquals(createItem(id = "2", value = BigDecimal(4), rate = 2.0), items[1])
    }

    @Test
    fun `update amount makes conversion and make it active`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1", 2.0), createRate("2", 0.5))
        ))

        val feature = createFeature()
        feature.accept(Wish.UpdateAmount("2", "1"))
        val items = feature.state.items!!
        assertEquals(createItem(id = "2", value = BigDecimal.ONE, rate = 0.5), items[0])
        assertEquals(createItem(id = "1", value = BigDecimal(4), rate = 2.0), items[1])
    }

    @Test
    fun `update amount to zero reset conversion`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1", 0.5), createRate("2", 2.0))
        ))

        val feature = createFeature()
        feature.accept(Wish.UpdateAmount("1", "1"))
        feature.accept(Wish.UpdateAmount("1", null))

        val items = feature.state.items!!
        assertEquals(createItem(id = "1", rate = 0.5), items[0])
        assertEquals(createItem(id = "2", rate = 2.0), items[1])
    }

    @Test
    fun `when amount updated news triggered`() {
        whenever(repository.invoke(any())).thenReturn(Observable.just(
                listOf(createRate("1"), createRate("2"))
        ))
        val feature = createFeature()
        val test = feature.news.test()
        feature.accept(Wish.UpdateAmount("1", "1"))
        test.assertValue {
            it == News.ActiveUpdated
        }
    }

    private fun createItem(id: CurrencyId, value: BigDecimal? = null, rate: Double = 1.0) =
            State.Item(createRate(id, rate), value)

    private fun createRate(id: CurrencyId, rate: Double = 1.0) =
            Rate(Currency(id, id, ""), base, BigDecimal(rate))

    private fun createFeature() = Feature(base, repository, subscribeScheduler, observeScheduler)


}
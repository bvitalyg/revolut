package pro.babichev.revolut

import io.reactivex.Observable
import io.reactivex.Observable.empty
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pro.babichev.revolut.provider.RatesRepository
import pro.babichev.revolut.models.CurrencyId
import pro.babichev.revolut.Feature.News
import pro.babichev.revolut.Feature.State
import pro.babichev.revolut.Feature.State.Item
import pro.babichev.revolut.Feature.Wish
import pro.babichev.revolut.models.Rate
import pro.babichev.revolut.util.Report
import pro.babichev.tools.fsm.*
import java.math.BigDecimal
import java.math.MathContext

internal class Feature(private val base: CurrencyId,
                       private val dataSource: RatesRepository,
                       private val subscribeScheduler: Scheduler = Schedulers.computation(),
                       private val observeScheduler: Scheduler = AndroidSchedulers.mainThread()
) : FSM<Wish, State, News> by FSM.create(
        initial = State(),
        bootstrapper = BootstrapperImpl(base, dataSource, subscribeScheduler, observeScheduler),
        actor = ActorImpl(base, subscribeScheduler, observeScheduler),
        newsPublisher = NewsPublisherImpl,
        reducer = ReducerImpl
) {

    sealed class Wish {
        data class UpdateAmount(val id: CurrencyId, val amount: CharSequence?) : Wish()
        internal class UpdateRates(val rates: List<Rate>) : Wish()
    }

    data class State(
            val items: List<Item>? = null
    ) {

        data class Item(
                val rate: Rate,
                val value: BigDecimal?
        )

    }

    sealed class News {
        object ActiveUpdated : News()
    }

    private sealed class Effect {
        data class ListUpdated(val items: List<Item>) : Effect()
    }

    private class BootstrapperImpl(
            private val base: CurrencyId,
            private val dataSource: RatesRepository,
            private val subscribeScheduler: Scheduler,
            private val observeScheduler: Scheduler
    ) : Bootstrapper<Wish> {

        override fun invoke(): Observable<out Wish> =
                dataSource(base)
                        .subscribeOn(subscribeScheduler)
                        .observeOn(observeScheduler)
                        .map { Wish.UpdateRates(it) }

    }

    private class ActorImpl(
            private val base: CurrencyId,
            private val subscribeScheduler: Scheduler,
            private val observeScheduler: Scheduler
    ) : Actor<State, Wish, Effect> {

        override fun invoke(state: State, wish: Wish): Observable<out Effect> = when (wish) {
            is Wish.UpdateAmount -> updateAmount(state.items, wish.id, wish.amount)
            is Wish.UpdateRates -> updateRates(state.items, wish.rates)
        }

        private fun updateAmount(items: List<Item>?, id: CurrencyId, amount: CharSequence?): Observable<out Effect> {
            if (items != null) {
                val oldCurrentItem = items.find { it.rate.currency.id == id }
                if (oldCurrentItem != null) {
                    return async {
                        val parsedAmount = amount.toSafeBigDecimal()
                        val newCurrentItem = Item(oldCurrentItem.rate, parsedAmount)

                        val amountInBaseCurrency = oldCurrentItem.rate.getAmountInBaseCurrency(parsedAmount)
                        items
                                .map {
                                    Item(
                                            rate = it.rate,
                                            value = it.rate.getAmountInLocalCurrency(amountInBaseCurrency)
                                    )
                                }
                                .raise(newCurrentItem)
                                .let { Effect.ListUpdated(it) }
                    }
                } else {
                    Report.throwOnDebug("Currency Id is not in the list: $id, $items")
                }
            } else {
                Report.throwOnDebug("Attempting to edit currency on uninitialized feature: $this, $id")
            }
            // prod fallback
            return empty()
        }

        private fun updateRates(items: List<Item>?, newRates: List<Rate>): Observable<out Effect> {
            val oldCurrentItem = items?.getOrNull(0) // if it's loaded then first list item
                    ?: newRates.find { it.currency.id == base }?.let { Item(it, null) } // on initial we take it from base currency
                    ?: newRates.getOrNull(0)?.let { Item(it, null) } // and fallback on the first

            if (oldCurrentItem != null) {
                val newCurrentRate = newRates.find { it.currency.id == oldCurrentItem.rate.currency.id }
                if (newCurrentRate != null) {
                    return async {
                        val newCurrentItem = Item(newCurrentRate, oldCurrentItem.value)
                        val amountInBaseCurrency = newCurrentRate.getAmountInBaseCurrency(newCurrentItem.value)

                        newRates
                                .map {
                                    Item(
                                            rate = it,
                                            value = it.getAmountInLocalCurrency(amountInBaseCurrency)
                                    )
                                }
                                .raise(newCurrentItem)
                                .let { Effect.ListUpdated(it) }
                    }
                } else {
                    Report.throwOnDebug("Currency Id is not in the list: $base, $items")
                }
            } else {
                Report.throwOnDebug("Attempting to update rates with empty list: $items, $newRates")
            }
            // prod fallback
            return empty()
        }

        private inline fun <T> async(crossinline init: () -> T) = Observable
                .fromCallable { init() }
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)

    }

    private object ReducerImpl : Reducer<State, Effect> {
        override fun invoke(state: State, effect: Effect): State = when (effect) {
            is Effect.ListUpdated -> state.copy(items = effect.items)
        }
    }

    private object NewsPublisherImpl : NewsPublisher<Wish, Effect, State, News> {
        override fun invoke(wish: Wish, effect: Effect, state: State): News? = when (wish) {
            is Wish.UpdateAmount -> when (effect) {
                is Effect.ListUpdated -> News.ActiveUpdated
            }
            else -> null
        }
    }

    private companion object {

        fun Rate.getAmountInLocalCurrency(amountInBaseCurrency: BigDecimal?) =
                amountInBaseCurrency?.multiply(rate, MathContext.DECIMAL128)

        fun Rate.getAmountInBaseCurrency(amount: BigDecimal?) =
                amount?.divide(rate, MathContext.DECIMAL128)

        @Suppress("NOTHING_TO_INLINE")
        inline fun CharSequence?.toSafeBigDecimal(): BigDecimal? =
                try {
                    if (!this.isNullOrEmpty()) {
                        BigDecimal(this.toString())
                    } else {
                        null
                    }
                } catch (e: Throwable) {
                    Report.throwOnDebug("Invalid BigDecimal input: $this")
                    // prod fallback
                    BigDecimal.ZERO
                }


        @Suppress("NOTHING_TO_INLINE")
        inline fun List<Item>.raise(item: Item) = this
                .toMutableList()
                .apply {
                    // to prevent unnecessary animations of switching between
                    // stack-sorted list from feature and alphabet sorted list from API
                    sortBy { it.rate.currency.id }

                    val raised = find { it.rate.currency.id == item.rate.currency.id }
                    if (raised != null && remove(raised)) {
                        add(0, item)
                    }
                }

    }

}
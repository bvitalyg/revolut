package pro.babichev.revolut.models

import java.math.BigDecimal

data class Rate(
        val currency: Currency,
        val base: CurrencyId,
        val rate: BigDecimal
)
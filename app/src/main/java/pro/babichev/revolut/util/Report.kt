package pro.babichev.revolut.util

import android.util.Log
import pro.babichev.revolut.BuildConfig

// this is a mock for errors reporting. For demo purposes it's an empty singleton,
// however, on real app interface injection would be better.
object Report {

    fun throwOnDebug(message: String? = null, e: Throwable? = null) {
//        if (e != null) {
//            Log.e("Report", message ?: "", e)
//        }  else {
//            Log.e("Report", message ?: "")
//        }
//        if (BuildConfig.DEBUG) {
//            throw RuntimeException(e)
//        }
    }

}
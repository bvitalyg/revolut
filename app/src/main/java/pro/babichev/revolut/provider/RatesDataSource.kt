package pro.babichev.revolut.provider

import io.reactivex.Observable
import pro.babichev.revolut.models.CurrencyId
import pro.babichev.revolut.provider.RatesDataSource.Rates
import pro.babichev.revolut.provider.RatesDataSource.Rates.Snapshot
import pro.babichev.revolut.provider.RatesDataSource.Rates.Error
import java.util.concurrent.TimeUnit

interface RatesDataSource : (CurrencyId) -> Observable<Rates> {

    override fun invoke(base: CurrencyId): Observable<Rates>

    sealed class Rates {

        data class Snapshot(
                val base: CurrencyId,
                val rates: Map<CurrencyId, Double>
        ) : Rates()

        data class Error(val error: Throwable) : Rates()

    }

    companion object {

        fun create(api: RatesApi,
                   interval: Int = 1,
                   unit: TimeUnit = TimeUnit.SECONDS
        ): RatesDataSource = Impl(api, interval, unit)

    }

    private class Impl(
            private val api: RatesApi,
            private val interval: Int,
            private val unit: TimeUnit
    ) : RatesDataSource {

        override fun invoke(base: CurrencyId) = Observable
                .interval(interval.toLong(), unit)
                .switchMapSingle {
                    api(base)
                            .map<Rates> {
                                Snapshot(
                                        base = it.base,
                                        rates = it.rates
                                                .toMutableMap()
                                                .apply {
                                                    // there's no base currency in original list so lets put it in here
                                                    put(it.base, 1.0)
                                                }
                                )
                            }
                            .onErrorReturn(::Error)
                }
    }

}
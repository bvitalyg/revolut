package pro.babichev.revolut.mapper

import pro.babichev.revolut.Feature
import pro.babichev.revolut.view.ItemView
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

internal object StateToViewModel : (Feature.State) -> List<ItemView.ViewModel> {

    private val decimalFormat = NumberFormat
            .getNumberInstance(Locale.getDefault())
            .let { it as DecimalFormat }
            .also { it.applyPattern("###.##") }

    override fun invoke(state: Feature.State): List<ItemView.ViewModel> = state
            .items
            ?.map {
                ItemView.ViewModel(
                        id = it.rate.currency.id,
                        name = it.rate.currency.title,
                        iconUri = it.rate.currency.iconUri,
                        amount = it.value?.let(decimalFormat::format)
                )
            } ?: emptyList()

}
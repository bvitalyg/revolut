package pro.babichev.revolut

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import pro.babichev.revolut.analytics.Analytics
import pro.babichev.revolut.mapper.EventToWish
import pro.babichev.revolut.mapper.StateToViewModel
import pro.babichev.revolut.models.CurrencyId
import pro.babichev.revolut.models.toHashLong
import pro.babichev.revolut.provider.RatesRepository
import pro.babichev.revolut.provider.CurrenciesFinder
import pro.babichev.revolut.provider.RatesApi
import pro.babichev.revolut.provider.RatesDataSource
import pro.babichev.revolut.view.ItemView
import pro.babichev.revolut.Feature.News
import pro.babichev.revolut.util.AutoAssignMaxRecycledViewPool
import pro.babichev.revolut.util.KeyboardFocusFixLayoutManager
import pro.babichev.tools.binder.Binder
import pro.babichev.tools.binder.to
import pro.babichev.tools.binder.using
import pro.babichev.tools.lister.ViewType
import pro.babichev.tools.lister.toListLister
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private val progress by lazy(LazyThreadSafetyMode.NONE) { findViewById<View>(R.id.progress) }

    private val recyclerView by lazy(LazyThreadSafetyMode.NONE) {
        findViewById<RecyclerView>(R.id.recycler)
                .apply {
                    layoutManager = KeyboardFocusFixLayoutManager(context)
                    setRecycledViewPool(AutoAssignMaxRecycledViewPool(20))
                }
    }

    private val lister by lazy(LazyThreadSafetyMode.NONE) {
        recyclerView.toListLister<ItemView.ViewModel>()
                // TODO: diff util produces weird animations here and scroll visual bugs. Disabled for now.
//                .itemsSameWhen { a, b -> a.id == b.id }
//                .contentsSameWhen { a, b -> a == b }
                .idProjection { it, _ -> it.id.toHashLong() }
                .events(ItemView.Event::class)
                .viewType(viewType)
    }

    private val feature = Feature(baseCurrency, ratesRepository)

    private val binder = Binder.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binder.bind(feature)
        binder.bind(lister using EventToWish to feature)
        binder.bind(lister to Analytics)
        binder.bind(feature using StateToViewModel to lister)
        binder.bind(feature.news to {
            when (it) {
                is News.ActiveUpdated -> recyclerView.scrollToPosition(0)
            }
        })
        binder.bind(feature to {
            progress.visibility = if (it.items == null) View.VISIBLE else View.GONE
        })
    }

    override fun onDestroy() {
        binder.dispose()
        super.onDestroy()
    }

    companion object {

        private const val baseCurrency: CurrencyId = "EUR"

        private val ratesDataSource = RatesDataSource.create(
                api = RatesApi.retrofit(),
                interval = 1,
                unit = TimeUnit.SECONDS
        )

        private val ratesRepository = RatesRepository.default(ratesDataSource, CurrenciesFinder.default())

        private val viewType = ViewType.create { ItemView(it) }

    }

}

package pro.babichev.revolut.models

typealias CurrencyId = String

internal fun CurrencyId.toHashLong(): Long {
    var result = 0L
    if (length > 3) {
        result += this[3].toLong() shl 48
    }
    if (length > 2) {
        result += this[2].toLong() shl 32
    }
    if (length > 1) {
        result += this[1].toLong() shl 16
    }
    if (length > 0) {
        result += this[0].toLong() shl 0
    }
    return result
}
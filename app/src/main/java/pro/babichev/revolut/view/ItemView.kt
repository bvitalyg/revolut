package pro.babichev.revolut.view

import android.support.annotation.LayoutRes
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.ObservableSource
import pro.babichev.revolut.R
import pro.babichev.revolut.models.CurrencyId
import pro.babichev.revolut.view.ItemView.Event
import pro.babichev.revolut.view.ItemView.ViewModel
import pro.babichev.tools.lister.ReactiveView

class ItemView(
        parent: ViewGroup,
        @LayoutRes private val layoutId: Int = R.layout.item_view,
        private val events: PublishRelay<Event> = PublishRelay.create()
) : ReactiveView<Event, ViewModel?>, ObservableSource<Event> by events {

    sealed class Event {
        data class Click(val item: ViewModel) : Event()
        data class AmountUpdated(val item: ViewModel, val amount: String?) : Event()
    }

    data class ViewModel(
            val id: CurrencyId,
            val name: CharSequence,
            val iconUri: CharSequence,
            val amount: CharSequence?
    )

    override val nativeView: View = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)

    private val firstLine = nativeView.findViewById<TextView>(R.id.firstLine)
    private val secondLine = nativeView.findViewById<TextView>(R.id.secondLine)
    private val icon = nativeView.findViewById<SimpleDraweeView>(R.id.icon)
    private val extra = nativeView.findViewById<EditText>(R.id.extra)

    private var item: ViewModel? = null

    private val textWatcher = object : TextWatcher {

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

        override fun afterTextChanged(s: Editable?) {
            item?.let {
                events.accept(Event.AmountUpdated(it, s?.toString()))
            }
        }
    }

    init {
        nativeView.setOnClickListener {
            item?.let {
                events.accept(Event.Click(it))
            }
        }
        extra.addTextChangedListener(textWatcher)
    }

    override fun accept(t: ViewModel?) {
        this.item = t
        if (t != null) {
            firstLine.text = t.id
            secondLine.text = t.name
            icon.setImageURI(t.iconUri.toString())

            extra.removeTextChangedListener(textWatcher)
            extra.setText(t.amount)
            extra.setSelection(extra.length())
            extra.addTextChangedListener(textWatcher )
        }
    }

}
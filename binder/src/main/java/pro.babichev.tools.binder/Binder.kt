package pro.babichev.tools.binder

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface Binder : Disposable {

    fun bind(disposable: Disposable?)

    fun clear()

    companion object {

        fun create(): Binder = object : Binder {

            val disposables = CompositeDisposable()

            override fun bind(disposable: Disposable?) {
                if (disposable != null) {
                    disposables.add(disposable)
                }
            }

            override fun clear() = disposables.clear()

            override fun isDisposed() = disposables.isDisposed

            override fun dispose() = disposables.dispose()
        }
    }
}

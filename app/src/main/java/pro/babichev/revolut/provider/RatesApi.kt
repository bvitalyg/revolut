package pro.babichev.revolut.provider

import com.jakewharton.retrofit2.converter.kotlinx.serialization.serializationConverterFactory
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import okhttp3.MediaType
import pro.babichev.revolut.provider.RatesApi.Response
import pro.babichev.revolut.models.CurrencyId
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import javax.xml.datatype.DatatypeConstants.SECONDS
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

interface RatesApi {

    @GET("latest")
    operator fun invoke(@Query("base") base: CurrencyId): Single<Response>

    @Serializable
    data class Response(

            @SerialName("base")
            val base: CurrencyId,

            @SerialName("date")
            val date: String,

            @SerialName("rates")
            val rates: Map<CurrencyId, Double>

    )

    companion object {

        private val okHttpClient = OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build()

        private val retrofit = Retrofit.Builder()
                .baseUrl("https://revolut.duckdns.org")
                .addConverterFactory(serializationConverterFactory(MediaType.get("application/json"), JSON))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.computation()))
                .client(okHttpClient)
                .build()

        fun retrofit(): RatesApi = retrofit.create(RatesApi::class.java)

    }
}
package pro.babichev.revolut.provider

import io.reactivex.Observable
import pro.babichev.revolut.models.Rate
import pro.babichev.revolut.models.CurrencyId
import java.math.BigDecimal

interface RatesRepository : (CurrencyId) -> Observable<List<Rate>> {

    override fun invoke(base: CurrencyId): Observable<List<Rate>>

    companion object {

        fun default(
                rates: RatesDataSource,
                finder: CurrenciesFinder
        ): RatesRepository = Impl(rates, finder)

    }

    private class Impl(
            private val rates: RatesDataSource,
            private val finder: CurrenciesFinder
    ) : RatesRepository {

        override fun invoke(base: CurrencyId): Observable<List<Rate>> =
                rates(base)
                        .filter { it is RatesDataSource.Rates.Snapshot }
                        .map {
                            it as RatesDataSource.Rates.Snapshot
                            it.rates.map { rate ->
                                Rate(
                                        currency = finder(rate.key),
                                        base = it.base,
                                        rate = BigDecimal(rate.value)
                                )
                            }
                        }

    }

}
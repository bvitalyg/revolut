package pro.babichev.revolut.provider

import com.mynameismidori.currencypicker.ExtendedCurrency
import pro.babichev.revolut.models.Currency
import pro.babichev.revolut.models.CurrencyId
import pro.babichev.revolut.util.Report
import java.lang.IllegalArgumentException

interface CurrenciesFinder : (CurrencyId) -> Currency {

    companion object {

        fun default(): CurrenciesFinder = Impl

    }

    private object Impl : CurrenciesFinder {

        override fun invoke(id: CurrencyId): Currency {
            val extendedCurrency = ExtendedCurrency.getCurrencyByISO(id)

            if (extendedCurrency != null) {
                return Currency(
                        id = id,
                        title = extendedCurrency.name,
                        iconUri = "res:///${extendedCurrency.flag}"
                )
            }

            val javaCurrency = getJavaCurrencySafely(id)
            if (javaCurrency != null) {
                return Currency(
                        id = id,
                        title = javaCurrency.displayName,
                        iconUri = ""
                )
            }

            Report.throwOnDebug("Unexpected currency ID $id")

            // prod fallback
            return Currency(
                    id = id,
                    title = id,
                    iconUri = ""
            )
        }

        private fun getJavaCurrencySafely(id: CurrencyId) =
                try {
                    java.util.Currency.getInstance(id)
                } catch (e: IllegalArgumentException) {
                    null
                }
    }
}
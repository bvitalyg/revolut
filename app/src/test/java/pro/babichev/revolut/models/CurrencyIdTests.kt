package pro.babichev.revolut.models

import org.junit.Assert.assertEquals
import org.junit.Test

class CurrencyIdTests {

    @Test
    fun `hash from empty is zero`() {
        assertEquals(0L, "".toHashLong())
    }

    @Test
    fun `hash from 1 char`() {
        assertEquals(0x0061L, "a".toHashLong())
    }

    @Test
    fun `hash from 2 chars`() {
        assertEquals(0x0062_0061L, "ab".toHashLong())
    }

    @Test
    fun `hash from 3 chars`() {
        assertEquals(0x0063_0062_0061L, "abc".toHashLong())
    }

    @Test
    fun `hash from 4 chars`() {
        assertEquals(0x0064_0063_0062_0061L, "abcd".toHashLong())
    }

    @Test
    fun `hash from 4+ chars`() {
        assertEquals(0x0064_0063_0062_0061L, "abcde".toHashLong())
    }

}
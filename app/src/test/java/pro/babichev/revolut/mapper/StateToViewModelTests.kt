package pro.babichev.revolut.mapper

import org.junit.Assert.*
import org.junit.Test
import pro.babichev.revolut.Feature
import pro.babichev.revolut.models.Currency
import pro.babichev.revolut.models.Rate
import java.math.BigDecimal

class StateToViewModelTests {

    private val rate = Rate(
            currency = Currency(
                    id = "GBP",
                    title = "Pounds",
                    iconUri = ""
            ),
            base = "EUR",
            rate = BigDecimal.ONE
    )

    @Test
    fun `when state is null ViewModel is empty`() {
        val state = Feature.State(items = null)
        assertTrue(StateToViewModel(state).isEmpty())
    }

    @Test
    fun `when value is null the amount is empty`() {
        val state = Feature.State(items = listOf(Feature.State.Item(
            rate = rate, value = null
        )))

        assertNull(StateToViewModel(state)[0].amount)
    }

    @Test
    fun `when value is integer the amount is integer`() {
        val state = Feature.State(items = listOf(Feature.State.Item(
            rate = rate, value = BigDecimal.TEN
        )))

        assertEquals("10", StateToViewModel(state)[0].amount)
    }

    @Test
    fun `when value is float with one fraction the amount has only once fraction`() {
        val state = Feature.State(items = listOf(Feature.State.Item(
            rate = rate, value = BigDecimal("1.1")
        )))

        assertEquals("1.1", StateToViewModel(state)[0].amount)
    }

    @Test
    fun `when value is float with two fractions the amount has only two fractions`() {
        val state = Feature.State(items = listOf(Feature.State.Item(
            rate = rate, value = BigDecimal("1.11")
        )))

        assertEquals("1.11", StateToViewModel(state)[0].amount)
    }

    @Test
    fun `when value is float with three fractions the amount has only two fractions`() {
        val state = Feature.State(items = listOf(Feature.State.Item(
            rate = rate, value = BigDecimal("1.111")
        )))

        assertEquals("1.11", StateToViewModel(state)[0].amount)
    }

    @Test
    fun `when value is float with three fractions the amount has only two fractions rounded`() {
        val state = Feature.State(items = listOf(Feature.State.Item(
            rate = rate, value = BigDecimal("1.119")
        )))

        assertEquals("1.12", StateToViewModel(state)[0].amount)
    }

    @Test
    fun `when value is float with many exponents the amount has same exponents`() {
        val state = Feature.State(items = listOf(Feature.State.Item(
            rate = rate, value = BigDecimal("123456789.11")
        )))

        assertEquals("123456789.11", StateToViewModel(state)[0].amount)
    }



}
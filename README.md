**Revolut Demo App**

This demo was designed to show modern reactive architecture approach.

---

## Core concepts

1. Every interaction is reactive. By so we mean every component represents bi-directional flow by implementing
```ObservableSource<Output>``` and ```Consumer<Input>```.
2. Even views are the same: ```ObservableSource<Event>``` and ```Consumer<ViewModel>```.
3. The projects uses [Lister](https://github.com/bvitaliyg/Lister), which is basically bi-directional wrapper around
```RecyclerView```, implementing ```Consumer<List<ViewModel>>``` and providing ```ObservableSource<ItemEventsSuperset>```.
It also handles all the jobs for adapters, holders, diff utils etc.
4. The project uses ```Binder```, which is a hard fork of Binder from [MVICore](https://github.com/badoo/MVICore).
Yet it's not extracted as a library so it's a submodule.
5. ```binder.bind(a to b) == Observable.wrap(a).subscribe(b)```,
```binder.bind(a using Function to b) == Observable.wrap(a).map(Function::invoke).subscribe(b)```,
6. The projects uses [FSM](https://github.com/bvitaliyg/FSM), which is a MVI state machine. The basic logic is:
    1. It's ```Consumer<Wish>``` -- which in an intent in terms of MVI.
    2. ```Wish``` is being delivered to ```Actor```, which is responsible to return an ```Observable``` stream of ```Effect```.
    3. ```Effect``` is a class describing an atomic change in ```State```.
    It's being passed to ```Reducer``` which provides a new ```State``` according to its changes.
    4. New ```State``` is being set for FSM, providing both ```ObservableSource<State>``` and ```FSM.state``` access.
    5. For one-time events (such as Toasts, etc) ```News``` can be emitted as well.
    6. ```Bootstrapper``` is an internal producer of ```Wish``` (which is used for repo sync for example).

Singe every component has it's own strictly-defined alphabet is's very easy to test it, reuse it and combine in any way.
```FSM``` provides a very good way to writing reliable business logic.
package pro.babichev.revolut.provider

import org.junit.Assert.assertEquals
import org.junit.Test
import pro.babichev.revolut.R
import pro.babichev.revolut.models.Currency

class CurrenciesFinderTests {

    private val currenciesFinder = CurrenciesFinder.default()

    @Test
    fun correct() {
        assertEquals(Currency(
                id = "UYU",
                title = "Uruguay Peso",
                iconUri = "res:///" + R.drawable.flag_uyu
        ), currenciesFinder("UYU"))
    }

    @Test
    fun notFound() {
        assertEquals(Currency(
                id = "KEK",
                title = "KEK",
                iconUri = ""
        ), currenciesFinder("KEK"))
    }

    @Test
    fun invalidArgument() {
        assertEquals(Currency(
                id = "KEKA",
                title = "KEKA",
                iconUri = ""
        ), currenciesFinder("KEKA"))
    }

}
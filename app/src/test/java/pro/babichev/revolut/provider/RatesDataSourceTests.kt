package pro.babichev.revolut.provider

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Test
import java.lang.RuntimeException
import java.util.concurrent.TimeUnit

class RatesDataSourceTests {

    private val success = "success"
    private val fail = "fail"
    private val errorException = RuntimeException("Error")

    private val api = mock<RatesApi> {

        whenever(it.invoke(eq(success))).thenReturn(
                Single.just(
                        createSuccessResponse(success)
                )
        )
        whenever(it.invoke(eq(fail))).thenReturn(
                Single.error(errorException)
        )
    }

    private val dataSource = RatesDataSource.create(
            interval = 10,
            unit = TimeUnit.MILLISECONDS,
            api = api
    )

    @Test
    fun `on correct response snapshot created`() {
        dataSource(success)
                .test()
                .awaitCount(1)
                .assertValueAt(0) {
                    it is RatesDataSource.Rates.Snapshot
                }
    }

    @Test
    fun `on correct response snapshot contains base`() {
        dataSource(success)
                .test()
                .awaitCount(1)
                .assertValueAt(0) {
                    (it as RatesDataSource.Rates.Snapshot).let {
                        it.rates[it.base] == 1.0
                    }
                }
    }

    @Test
    fun `on incorrect response error created`() {
        dataSource(fail)
                .test()
                .awaitCount(1)
                .assertValueAt(0) {
                    it is RatesDataSource.Rates.Error
                }
    }

    @Test
    fun `on incorrect response error contains exception`() {
        dataSource(fail)
                .test()
                .awaitCount(1)
                .assertValueAt(0) {
                    (it as RatesDataSource.Rates.Error).error === errorException
                }
    }

    @Test
    fun `with 2 responses running ok result is emitted twice`() {
        reset(api)
        var count = 0
        whenever(api.invoke(any())).thenAnswer {
            count++
            if (count == 1) {
                Single.just(createSuccessResponse("1"))
            } else {
                Single.just(createSuccessResponse("2"))
            }
        }

        dataSource(success)
                .test()
                .awaitCount(2)
                .assertValueAt(0) {
                    (it as RatesDataSource.Rates.Snapshot).base == "1"
                }
                .assertValueAt(1) {
                    (it as RatesDataSource.Rates.Snapshot).base == "2"
                }
    }

    private fun createSuccessResponse(base: String): RatesApi.Response {
        return RatesApi.Response(
                base = base,
                date = "date",
                rates = mapOf(
                        "1" to 1.0,
                        "2" to 2.0,
                        "3" to 3.0
                )
        )
    }


}
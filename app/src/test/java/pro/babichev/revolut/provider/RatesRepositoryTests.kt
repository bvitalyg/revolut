package pro.babichev.revolut.provider

import com.jakewharton.rxrelay2.PublishRelay
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import pro.babichev.revolut.models.Currency
import pro.babichev.revolut.models.CurrencyId
import pro.babichev.revolut.models.Rate
import java.math.BigDecimal

class RatesRepositoryTests {

    private val dataSourceSubject = PublishRelay.create<RatesDataSource.Rates>()

    private val finder = mock<CurrenciesFinder> {
        whenever(it.invoke(eq("1"))).thenReturn(
                Currency("1", "1", "")
        )
        whenever(it.invoke(eq("2"))).thenReturn(
                Currency("2", "2", "")
        )
    }

    private val datasource = mock<RatesDataSource> {
        whenever(it.invoke(any())).thenReturn(dataSourceSubject)
    }

    private val reporitory = RatesRepository.default(
            datasource,
            finder
    )

    @Test
    fun `repository passes snapshots`() {
        val test = reporitory.invoke("").test()

        val snapshot1 = RatesDataSource.Rates.Snapshot("b", mapOf("1" to 1.0))
        val snapshot2 = RatesDataSource.Rates.Snapshot("b", mapOf("2" to 1.0))

        dataSourceSubject.accept(snapshot1)
        dataSourceSubject.accept(snapshot2)

        test
                .awaitCount(2)
                .assertValueAt(0) { it == mockResponse("1") }
                .assertValueAt(1) { it == mockResponse("2") }

    }

    @Test
    fun `repository blocks errors`() {
        val test = reporitory.invoke("").test()

        val snapshot = RatesDataSource.Rates.Snapshot("b", mapOf("1" to 1.0))
        val error = RatesDataSource.Rates.Error(RuntimeException())

        dataSourceSubject.accept(snapshot)
        dataSourceSubject.accept(error)

        test
                .awaitCount(1)
                .assertValueAt(0) { it == mockResponse("1") }

    }

    private fun mockResponse(id: CurrencyId) =
        listOf(Rate(Currency(id, id, ""), "b", BigDecimal(1.0)))


}

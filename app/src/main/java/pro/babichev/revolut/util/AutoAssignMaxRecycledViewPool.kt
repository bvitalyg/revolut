package pro.babichev.revolut.util

import android.support.v7.widget.RecyclerView
import android.util.SparseIntArray

// twicked pool for RecyclerView
class AutoAssignMaxRecycledViewPool(private val maxSize: Int) : RecyclerView.RecycledViewPool() {

    private val maxScrap = SparseIntArray()

    override fun setMaxRecycledViews(viewType: Int, max: Int) {
        maxScrap.put(viewType, max)
        super.setMaxRecycledViews(viewType, max)
    }

    override fun putRecycledView(scrap: RecyclerView.ViewHolder) {
        val viewType = scrap.itemViewType
        val max = maxScrap.get(viewType, -1)
        if (max == -1) {
            setMaxRecycledViews(viewType, maxSize)
        }
        super.putRecycledView(scrap)
    }

    override fun clear() {
        maxScrap.clear()
        super.clear()
    }

}
package pro.babichev.revolut.models

data class Currency(
        val id: CurrencyId,
        val title: String,
        val iconUri: String
)
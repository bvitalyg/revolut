package pro.babichev.tools.binder

import io.reactivex.*
import io.reactivex.annotations.Nullable
import io.reactivex.internal.fuseable.HasUpstreamObservableSource
import io.reactivex.internal.fuseable.QueueFuseable
import io.reactivex.internal.observers.BasicFuseableObserver

// this is a custom operator for better performance based on RxJava Map operator implementation.
internal class ObservableFilterMap<In, Out>(
        private val source: ObservableSource<In>,
        private val function: (In) -> Out?
) : Observable<Out>(), HasUpstreamObservableSource<In> {

    override fun source() = source

    public override fun subscribeActual(t: Observer<in Out>) {
        source.subscribe(MapFilterObserver(t, function))
    }

    private class MapFilterObserver<In, Out>(
            actual: Observer<in Out>,
            private val mapper: (In) -> Out?
    ) : BasicFuseableObserver<In, Out>(actual) {

        override fun onNext(t: In) {
            if (done) {
                return
            }

            if (sourceMode != QueueFuseable.NONE) {
                downstream.onNext(null)
                return
            }

            val v: Out?

            try {
                v = mapper(t)
            } catch (ex: Throwable) {
                fail(ex)
                return
            }

            if (v != null) {
                downstream.onNext(v)
            }
        }

        override fun requestFusion(mode: Int) = transitiveBoundaryFusion(mode)

        @Nullable
        @Throws(Exception::class)
        override fun poll(): Out? = qd.poll()?.let(mapper)

    }
}

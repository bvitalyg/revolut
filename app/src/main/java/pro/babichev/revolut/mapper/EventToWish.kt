package pro.babichev.revolut.mapper

import pro.babichev.revolut.Feature.Wish
import pro.babichev.revolut.view.ItemView.Event

internal object EventToWish : (Event) -> Wish {

    override fun invoke(event: Event): Wish = when (event) {
        is Event.Click -> Wish.UpdateAmount(event.item.id, event.item.amount)
        is Event.AmountUpdated -> Wish.UpdateAmount(event.item.id, event.amount)
    }

}
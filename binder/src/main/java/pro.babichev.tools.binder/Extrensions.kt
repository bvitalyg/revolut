package pro.babichev.tools.binder

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.plugins.RxJavaPlugins

@Suppress("NOTHING_TO_INLINE")
inline operator fun Binder.plusAssign(disposable: Disposable?) = bind(disposable)

@Suppress("NOTHING_TO_INLINE")
inline operator fun Binder.invoke(what: Binder.() -> Unit) {
    clear()
    try {
        what()
    } catch (t: Throwable) {
        clear()
        throw t
    }
}

@Suppress("NOTHING_TO_INLINE")
inline infix fun <T> ObservableSource<T>.to(consumer: Consumer<T>): Disposable = Observable.wrap(this).subscribe(consumer)

@Suppress("NOTHING_TO_INLINE")
inline infix fun <T> ObservableSource<T>.to(crossinline consumer: (T) -> Unit): Disposable = Observable.wrap(this).subscribe {
    consumer(it)
}

@Suppress("NOTHING_TO_INLINE")
infix fun <T, D> ObservableSource<T>.using(mapper: (T) -> D?): Observable<D> =
        Observable.wrap(this)
                .let {
                    RxJavaPlugins.onAssembly(ObservableFilterMap<T, D>(this, mapper))
                }